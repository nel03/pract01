/*events*/
/*Date()
	>set the actual Month, day, year and time
*/
let event = Date();

console.log(event);

// concat() joins two or more strings
let text1 = "Ronel";
let text2 = "Tayawa"
let text3 = text1.concat(" ",text2);

console.log(text3);


//  Number() Convert a date to a number
let x = new Date("1970-01-02");
console.log(Number(x));

// mini activity of s16

//console.log("hello");
/*
	1. Debug the following code to return the correct value and mimic the output.
*/

	let num1 = 25;
	let num2 = 5;
	console.log("The result of num1 + num2 should be 30.");
	console.log("Actual Result:");
	console.log(num1 + num2);

	let num3 = 156;
	let num4 = 44;
	console.log("The result of num3 + num4 should be 200.");
	console.log("Actual Result:");
	console.log(num3 + num4);

	let num5 = 17;
	let num6 = 10;
	console.log("The result of num5 - num6 should be 7.");
	console.log("Actual Result:");
	console.log(num5-num6);
		
/*

	2. Given the values below, calculate the total number of minutes in a year and save the result in a variable called resultMinutes.

*/
	let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365;

	let totalMinutes = minutesHour * hoursDay * daysYear
	console.log('there are ' + totalMinutes + ' minutes in a year.');
/*
	3. Given the values below, calculate and convert the temperature from celsius to fahrenheit and save the result in a variable called resultFahrenheit.
*/
	let tempCelsius = 132;
	const fahrenheit = ((tempCelsius * 9/5) + 32);
	console.log('132 Celsius when converted to Fahrenheit is ' + fahrenheit);

/*
	4a. Given the values below, identify if the values of the following variable are divisible by 8.
	   -Use a modulo operator to identify the divisibility of the number to 8.
	   -Save the result of the operation in an appropriately named variable.
	   -Log the value of the remainder in the console.
	   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy8
	   -Log a message in the console if num7 is divisible by 8.
	   -Log the value of isDivisibleBy8 in the console.

*/
	let num7 = 165;
	const remainder8 = 165 % 8;
	console.log('the remainder of 165 divided by 8 is: ' + remainder8);
	//Log the value of the remainder in the console.
	console.log("Is num7 divisible by 8?");
	const idDivisibileBy8 = remainder8 === 0;
	console.log(idDivisibileBy8);
	//Log the value of isDivisibleBy8 in the console.


/*
	4b. Given the values below, identify if the values of the following variable are divisible by 4.
	   -Use a modulo operator to identify the divisibility of the number to 4.
	   -Save the result of the operation in an appropriately named variable.
	   -Log the value of the remainder in the console.
	   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy4
	   -Log a message in the console if num8 is divisible by 4.
	   -Log the value of isDivisibleBy4 in the console.

*/
	let num8 = 348;
	const remainder4 = 348 % 4;
	console.log('the remainder of 348 divided by 4 is: ' + remainder4);
	//Log the value of the remainder in the console.
	console.log("Is num8 divisible by 4?");
	const isDivisibleBy4 = remainder4 === 0;
	console.log(isDivisibleBy4);
	//Log the value of isDivisibleBy4 in the console.


/*
Activity:
1. In the S16 folder, create an activity folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Copy the activity code from Boodle Notes and paste it into your index.js for the code and instructions.
4. Debug the given code to return the correct value and mimic the output. 
    Check the value’s data type.
    Check the if the operator used is correct.
5. Given the values in the code, calculate the total number of minutes in a year and save the result in a variable called resultMinutes.
    Log the result in the console.
6. Given the values in the code, calculate and convert the temperature from celsius to fahrenheit and save the result in a variable called resultFahrenheit.
    Log the value of the variable in the console.
7. Given the values in the code, identify if the values of the following variable are divisible by 8.
    Use a modulo operator to identify the divisibility of the number to 8.
    Save the result of the operation in an appropriately named variable.
    Log the value of the remainder in the console.
    Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy8.
    Log a message in the console if the number is divisible by 8.
    Log the value of isDivisibleBy8 in the console.
8. Given the values below, identify if the values of the following variable are divisible by 4.
    Use a modulo operator to identify the divisibility of the number to 4.
    Save the result of the operation in an appropriately named variable.
    Log the value of the remainder in the console.
    Using the strict equality operator, check if the remainder is equal to 0. 
    Save the returned value of the comparison in a variable called isDivisibleBy4. 
    Log a message in the console if the number is divisible by 4.
    Log the value of isDivisibleBy4 in the console.
9. Create a git repository named S16.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.
*/


// 1. In the S15 folder, create an activity folder, an index.html file inside of it and link the index.js file.
// 2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
// 3. Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:

const firstNumber = prompt("enter frst number");
const secondNumber = prompt('enter second number');

const total = firstNumber + secondNumber;

let result;
// - If the total of the two numbers is less than 10, add the numbers
// - If the total of the two numbers is 10 - 20, subtract the numbers
// - If the total of the two numbers is 21 - 30 multiply the numbers
// - If the total of the two numbers is greater than or equal to 30, divide the numbers
if (total < 10) {
	result = total;
} else if (total >= 10 && total < 20) {
	result = firstNumber - secondNumber
} else if (total >= 21 && total <30) {
	result = firstNumber * secondNumber
} else if (total > 30) {
	result = firstNumber / secondNumber
}

// 4. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.

if (total >= 10) {
	alert('total is: ' + total)
} else if(total < 10) {
	console.log('total is: ' + total);
}

// 5. Prompt the user for their name and age and print out different alert messages based on the user input:
// -  If the name OR age is blank/null, print the message are you a time traveler?
// -  If the name AND age is not blank, print the message with the user’s name and age.
const userName = prompt('enter your name');
const userAge = prompt('enter your age');

if (userName ===   userAge ===   userName === null || userAge === null) {
	console.log('are you a time traveler?')
} else if (userName && userAge) {
	console.log('Name: ' + userName + ' Age: ' + userAge);
}

// 6. Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
// - 18 or greater, print an alert message saying You are of legal age.
// - 17 or less, print an alert message saying You are not allowed here.

function checkisLegalAge (userAge){
	if (userAge >= 18){
		console.log('You are of legal age');
	} else {
		console.log ('You are not allowed here');
	}
}

// 8. Create a switch case statement that will check if the user's age input is within a certain set of expected input:
// - 18 - print the message You are now allowed to party.
// - 21 - print the message You are now part of the adult society.
// - 65 - print the message We thank you for your contribution to society.
// - Any other value - print the message Are you sure you're not an alien?

switch (userAge) {
	case 18:
	console.log('You are now allowed to party');
	break;
	case 21:
	console.log('Your are now part of the adult society');
	break;
	case 65:
	console.log('we thank you for your contribution to society');
	break;
	default:
	console.log("are you sure you're not an alien?")
}

// 8. Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.

